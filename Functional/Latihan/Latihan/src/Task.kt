fun main() {
    val text = "Kotlin".getFirstAndLast()

    val firstChar = text["first"]
    val lastChar = text["last"]

    printFirstAndLast(firstChar, lastChar)

}

fun printFirstAndLast(firstChar: Char?, lastChar: Char?){
    println("First letter is $firstChar and $lastChar for second letter")
}
fun String.getFirstAndLast(): Map<String, Char>{
    return mapOf("first" to this.first(),
            "last" to this.last())
}