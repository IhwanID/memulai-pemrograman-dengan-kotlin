// Coming Soon
fun main() {
    val stringResult = getResult("Kotlin")
    val intResult = getResult(100)

    println("""
        String result: $stringResult
        Int result: $intResult
    """.trimIndent())
}

fun <T> getResult(args: T): Int {
    when(args){
        is Int -> return args * 5
        is String -> return args.length
    }
    return  0
}